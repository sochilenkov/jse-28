package ru.t1.sochilenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.IUserOwnedRepository;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
